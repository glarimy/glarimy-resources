#include <iostream>
#include <vector> 

using namespace std;

class Comparable {
    public:
        virtual bool compare(Comparable* other) = 0;
};

class Contact : public Comparable{
    private: 
        string name;
        long phone;
    public:
        Contact(string name, long phone) {
            this->name = name;
            this->phone = phone;
        }
        string getName() {
            return this->name;
        }

        long getPhone() {
            return this->phone;
        }
        bool compare(Comparable* other) {
            Contact* c = (Contact*)other;
            if(this->getPhone() < c->getPhone()){
                return true;
            }
            
            return false;
        }
};

class Sorter {
    public:
        virtual void sort(Comparable* data, int size) = 0;
};

class BubbleSorter : public Sorter {
    public: 
        void sort(Comparable* data, int size) {
            for(int i=0; i<size; i++) {
                for (int j=0; j<size; j++) {
                    if((data+j)->compare(data+j)) {
                        Comparable* temp = data+j;
                        *(data + j) = *(data + j + 1);
                        *(data + j + 1) = *temp;
                    }
                }
            }
        }
};



class Repository {
    public: 
        virtual bool save(Contact contact) = 0;
        virtual Contact* findAll() = 0;
};

class InMemoryRepository : public Repository{
    private:
        static InMemoryRepository* INSTANCE;
        vector<Contact> cache; 

    public: 
        static InMemoryRepository* getInstance() {
            if (INSTANCE == NULL)
                INSTANCE = new InMemoryRepository();
            return INSTANCE;
        }
        bool save(Contact contact){
            cache.push_back(contact);
            return true;
        }
        Contact* findAll() {
            return cache.data();
        }
};

InMemoryRepository* InMemoryRepository::INSTANCE = NULL;

class RepositoryFactory {
    public:
        static Repository* getRepository(string key) {
            if(key == "basic")
                return InMemoryRepository::getInstance();
            return NULL;
        }
};

class Directory {
    public: 
        virtual bool add(Contact contact) = 0;
        virtual Contact* list() = 0;
};

class SimpleDirectory : public Directory{
    private: 
        Repository* repo;
    public: 
        SimpleDirectory() {
            repo = RepositoryFactory::getRepository("basic");
        }
        bool add(Contact contact) {
            return repo->save(contact);
        }
        Contact* list() {
            return repo->findAll();
        }
};

class Logger : public Directory{
    private: 
        Directory* target;
    public: 
        Logger(Directory* target){
            this->target = target;
        }

        bool add(Contact contact) {
            cout << "add::entering" << endl;
            bool status = target->add(contact);
            cout << "add::exiting" << endl;
            return status;
        }

        Contact* list() {
            cout << "list::entering" << endl;
            Contact* contacts = target->list();
            cout << "list::exiting" << endl;
            return contacts;
        }
};

class Validator  : public Directory{
    private: 
        Directory* target;
    public: 
        Validator(Directory* target){
            this->target = target;
        }

        bool add(Contact contact) {
            if(contact.getName() == "")
                return false;
            return target->add(contact);
        }

        Contact* list() {
            return target->list();
        }
};

class SortableDirectory : Directory {
    private: 
        Directory* target;
    public:
        SortableDirectory(Directory* target) {
            this->target = target;
        }
        
        bool add(Contact contact) {
            return target->add(contact);
        }

        Contact* list() {
            return target->list();
        }

        Contact* sortedList() {
            Sorter* sorter = new BubbleSorter();
            Contact* list = target->list();
            sorter->sort(list, 3);
            return target->list();
        }
};

class DirectoryFactory {
    public:
        static Directory* getDirectory(bool logging, bool validation){
            Directory* directory = new SimpleDirectory();
            
            if(validation == true){
                Directory* validator = new Validator(directory);
                if(logging == true)
                    return new Logger(validator);
                return validator;
            } else if(logging == true)
                return new Logger(directory);

            return directory;
        }
};

int main(int argc, char** argv)
{
    if(argc < 3) {
        cout << "Usage: a.out true|false true|false" << endl;
        return 0;
    }
    bool logging = strncmp(argv[1], "true", 4) == 0 ? true: false;
    bool validation = strncmp(argv[2], "true", 4) == 0 ? true: false;

    Directory* directory = DirectoryFactory::getDirectory(logging, validation);
    directory->add(Contact("Koyya", 973142317));    
    directory->add(Contact("Krishna", 973142316));
    directory->add(Contact("Mohan", 973142315));
    SortableDirectory* sd = new SortableDirectory(directory);
    return 0;
}