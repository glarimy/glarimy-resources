#include <iostream>
#include <vector> 

using namespace std;

class Contact {
    private: 
        string name;
    public:
        Contact(string name) {
            this->name = name;
        }
        string getName() {
            return this->name;
        }
};

class Repository {
    public: 
        virtual bool save(Contact contact) = 0;
};

class InMemoryRepository : public Repository{
    private:
        static InMemoryRepository* INSTANCE;
        vector<Contact> cache; 
        InMemoryRepository(){

        }
    public: 
        static InMemoryRepository* getInstance() {
            if (INSTANCE == NULL)
                INSTANCE = new InMemoryRepository();
            return INSTANCE;
        }
        bool save(Contact contact){
            cache.push_back(contact);
            return true;
        }
};

InMemoryRepository* InMemoryRepository::INSTANCE = NULL;

class RepositoryFactory {
    public:
        static Repository* getRepository(string key) {
            if(key == "basic")
                return InMemoryRepository::getInstance();
            return NULL;
        }
};

class Directory {
    public: 
        virtual bool add(Contact contact) = 0;
};

class SimpleDirectory : public Directory{
    private: 
        Repository* repo;
    public: 
        SimpleDirectory() {
            repo = RepositoryFactory::getRepository("basic");
        }
        bool add(Contact contact) {
            return repo->save(contact);
        }
};

class Logger : public Directory{
    private: 
        Directory* target;
    public: 
        Logger(Directory* target){
            this->target = target;
        }

        bool add(Contact contact) {
            cout << "adding contact" << endl;
            bool status = target->add(contact);
            cout << "added contact" << endl;
            return status;
        }
};

class Validator  : public Directory{
    private: 
        Directory* target;
    public: 
        Validator(Directory* target){
            this->target = target;
        }

        bool add(Contact contact) {
            if(contact.getName() == "")
                return false;
            return target->add(contact);
        }
};



class DirectoryFactory {
    public:
        static Directory* getDirectory(bool logging, bool validation){
            Directory* directory = new SimpleDirectory();
            
            if(validation == true){
                Directory* validator = new Validator(directory);
                if(logging == true)
                    return new Logger(validator);
                return validator;
            } else if(logging == true)
                return new Logger(directory);

            return directory;
        }
};


int main(int argc, char** argv)
{
    bool logging = strncmp(argv[1], "true", 4) == 0 ? true: false;
    bool validation = strncmp(argv[2], "true", 4) == 0 ? true: false;

    Directory* directory = DirectoryFactory::getDirectory(logging, validation);
    Contact contact = Contact("Krishna");
    bool status = directory->add(contact);
    cout << "Contact added successfully: " << status << endl;
    return 0;
}